package game

type GameResult struct {
	Direction DirectionType
	Result    int
}
