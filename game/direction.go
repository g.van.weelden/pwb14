package game

type DirectionType string

const (
	NORTH = "n"
	EAST  = "e"
	WEST  = "w"
	SOUTH = "s"
)

var DirectionMapping = map[int]DirectionType{
	0: NORTH,
	1: EAST,
	2: WEST,
	3: SOUTH}
