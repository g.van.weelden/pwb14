package game

import (
	"log"
	"math"
	"math/rand"
	"strconv"
	"strings"
	"time"
)

const ZEROFIELD = 0

type GameField struct {
	input string
	Field [][]int
	Size  int
}

func newGameField() *GameField {
	g := &GameField{}
	return g
}

func (g *GameField) Parse(input string) error {
	var err error
	list := strings.Split(input, ",")
	results := make(map[int]int64)

	for id, _ := range list {
		list[id] = strings.Trim(list[id], "([ ])")
		results[id], err = strconv.ParseInt(list[id], 10, 32)
		if err != nil {
			return err
		}
	}
	g.Size = int(math.Sqrt(float64(len(results))))
	g.Field = make([][]int, g.Size)
	for i := 0; i < g.Size; i++ {
		g.Field[i] = make([]int, g.Size)
		for j := 0; j < g.Size; j++ {
			g.Field[i][j] = int(results[i*g.Size+j])
		}
	}

	log.Printf("%q", list)
	log.Printf("%+v", g.Field)
	return nil
}

func (g *GameField) Calculate() DirectionType {
	resultList := make(map[int]DirectionType)
	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	resultChan := make(chan GameResult)
	go func(c chan GameResult) {
		c <- GameResult{Result: 0, Direction: NORTH}
	}(resultChan)
	select {
	case result := <-resultChan:
		resultList[result.Result] = result.Direction
	}
	return DirectionMapping[r.Intn(4)]
}
