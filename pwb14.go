/* pwb14.go */
package main

import (
	"bufio"
	"fmt"
	"guus/pwb14/game"
	"log"
	"os"
)

func main() {
	scanner := bufio.NewScanner(os.Stdin)
	gf := new(game.GameField)
	for scanner.Scan() {
		inputString := scanner.Text()
		//	log.Println(inputString) // Println will add back the final '\n'
		gf.Parse(inputString)
		fmt.Print(gf.Calculate()) // Println will add back the final '\n'
	}
	if err := scanner.Err(); err != nil {
		log.Printf("reading standard input: %s", err)
	}
}
